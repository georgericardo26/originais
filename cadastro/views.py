from django.http import HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import viewsets
from api.serializers import cadastroSerializers
from cadastro.forms import CadastroForm
from cadastro.models import Cadastro


class CadastroView(View):
    form_class = CadastroForm
    template_name = 'cadastro_form.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'form': self.form_class()})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        sucess = False
        
        if form.is_valid():
            sucess = True
            form.save()
            form = self.form_class()

        return render(request, self.template_name, {
            'form': form,
            'sucess': True
        })


# class CadastroView(ListView):
#     model = Cadastro
#     template_name = 'cadastro.html'
#     queryset = Cadastro.objects.all()
#     context_object_name = 'cadastro'
#
#
# class CadastroCreateView(CreateView):
#     model = Cadastro
#     template_name = 'cadastro_form.html'
#     form_class = CadastroForm
#     success_url = reverse_lazy('cadastro')
#
#
# class CadastroUpdateView(UpdateView):
#     model = Cadastro
#     template_name = 'cadastro_form_edit.html'
#     fields = ['nome_completo','visitante','idade','telefone','email', 'instagram',]
#     success_url = reverse_lazy('cadastro')
#
#
# class CadastroDeleteView(DeleteView):
#     model = Cadastro
#     template_name = 'cadastro_form_delete.html'
#     success_url = reverse_lazy('cadastro')
#
#
# class CadastroViewlist(ListView):
#     model = Cadastro
#     template_name = 'cadastrolist.html'
#     queryset = Cadastro.objects.all()
#     context_object_name = 'cadastro'
#
#
# def home(request):
#     return render(request, 'index.html')
#
#
# # Api
# class CadastroViewApi(viewsets.ModelViewSet):
#     queryset = Cadastro.objects.all()
#     serializer_class = cadastroSerializers