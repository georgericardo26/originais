from django.contrib import admin

# Register your models here.
from cadastro.models import Cadastro


@admin.register(Cadastro)
class AdminCadastro(admin.ModelAdmin):
    fields = ['nome_completo', 'idade', 'email', 'telefone', 'visitante']
    readonly_fields = ('created_at',)
    list_display = ('nome_completo', 'idade', 'email', 'telefone', 'visitante')
