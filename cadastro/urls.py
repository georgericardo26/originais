from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import CadastroView

# cadastroUrl = SimpleRouter()
# cadastroUrl.register('cadastroApi', CadastroViewApi)

urlpatterns = [
    # path('cadastroApi/', CadastroViewApi, name='cadastroApi'),
    # API
    # path('', home, name='home'),
    path('', CadastroView.as_view(), name='cadastro')
#     path('cadastroAdicionar/', CadastroCreateView.as_view(), name='cadastroAdicionar'),
#     path('<int:pk>/cadastroAtualizar/', CadastroUpdateView.as_view(), name='cadastroAtualizar'),
#     path('<int:pk>/cadastroDeletar/', CadastroDeleteView.as_view(), name='cadastroDeletar'),
#     path('cadastroLista/', CadastroViewlist.as_view(), name='cadastroLista'),
]
