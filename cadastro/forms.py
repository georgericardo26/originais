from django.forms import ModelForm
from cadastro.models import Cadastro


class CadastroForm(ModelForm):
    
    class Meta:
        model = Cadastro
        fields = [
            "nome_completo",
            "idade",
            "email",
            "instagram",
            "telefone",
            "visitante"
        ]
         

