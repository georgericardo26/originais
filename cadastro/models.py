from datetime import datetime
from django.db import models
# Create your models here.


class Cadastro(models.Model):

    YES_OR_NOT = [
        ('Sim','Sim'),
        ('Não','Não')
    ]
    
    AGE = [
        (age, age) for age in range(0, 100)
    ]

    nome_completo = models.CharField(verbose_name='Nome Completo', max_length=200, blank=False, null=False,)
    idade = models.IntegerField(choices=AGE)
    email = models.EmailField(verbose_name='E-mail', max_length=200, blank=True, null=True, unique=True)
    instagram = models.CharField(max_length=200, blank=True, null=True, unique=True)
    telefone = models.IntegerField(blank=True, null=True, unique=True)
    visitante = models.CharField(max_length=3, choices=YES_OR_NOT, blank=True, null=True)

    created_at = models.DateField(verbose_name='Data Criaçãoo', default=datetime.now, blank=True)
    updated_at = models.DateTimeField(verbose_name='Atualizado', auto_now=True)

    class Meta:
        verbose_name = 'Cadastro'
        verbose_name_plural = 'Cadastros'
        ordering = ['nome_completo']

    def __str__(self):
        return self.nome_completo