from rest_framework import serializers
from cadastro.models import Cadastro

class cadastroSerializers(serializers.ModelSerializer):
    class Meta:
        model = Cadastro
        fields = ('__all__')